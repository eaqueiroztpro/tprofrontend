
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { UserService,User } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form:FormGroup
  public invalidLogin:boolean=false;

  constructor(private formBuilder:FormBuilder,
              private userService:UserService,
              private router: Router,
              private cookieService:CookieService) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({      
      email:'',
      password:''
    });
  }

  submit():void{
    let user = this.form.getRawValue() as User;
    this.userService.login(user)    
      .subscribe((res)=> {  
        this.cookieService.set('myJwt',(res as any).jwt);
        this.router.navigate(["/"]);
      }, (err)=>{
        this.invalidLogin = true;
      });
  }
}
