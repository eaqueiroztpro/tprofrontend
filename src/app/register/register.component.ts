import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form:FormGroup;

  constructor(private formBuilder:FormBuilder,
              private userService:UserService,
              private router: Router) { 
     }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name:'',
      email:'',
      password:''
    });
  }
  submit():void{
    this.userService.registerUser(this.form.getRawValue())
      .subscribe(()=> this.router.navigate(["/login"]));

  }

}
