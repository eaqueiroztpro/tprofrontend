import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private API_TODO_URL="http://localhost:3000/todos"


  constructor(private http: HttpClient,
              private cookieService:CookieService) { 
  }

  private getHeader(){
    let jwt= this.cookieService.get('myJwt');
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${jwt}`
    });
    
    return { headers: headers };
  }

  getAll(){
    return this.http.get(this.API_TODO_URL +'/getAll');    
  }

  getCurrentUser(){
    return this.http.get(this.API_TODO_URL +'/getLoggedUser');    
  }

  updateTodo(todoItem:TodoItem){ 
    let headers = this.getHeader();   
    let promisse= this.http.put(this.API_TODO_URL +'/update',todoItem,headers);   
    return promisse;
  }

  registerTodo(todoItem:TodoItem){    
    let headers = this.getHeader();
    let promisse = this.http.post(this.API_TODO_URL +'/create',todoItem,headers);    
    return promisse;
  }

  removeTodo(todoId:string){    
    let headers = this.getHeader();
    let promisse= this.http.delete(this.API_TODO_URL +'/delete/' + todoId,headers);
        promisse.subscribe(()=> console.log('todo Updated!'));    
    return promisse;
  }

}

export interface TodoItem{
  id: string,
  description: string,
  done: boolean
}
