import { HttpClient } from '@angular/common/http';
import { Injectable,OnInit } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private API_USER_URL="http://localhost:3000/user"
  


  constructor(private http: HttpClient) { 
    
  }

  ngOnInit(): void {    
  }

  login(user:User){    
    return this.http.post(this.API_USER_URL +'/login', user, {withCredentials:true});
  }

  logout(){
    return this.http.post(this.API_USER_URL +'/logout', {}, {withCredentials:true})
  }

  getLoggedUser(){    
    return this.http.get(this.API_USER_URL +'/getLoggedUser', {withCredentials:true})
  }

  registerUser(user:User){    
    return this.http.post(this.API_USER_URL +'/register',user, {withCredentials:true})
  }
}

export interface User{
  name: string,
  email: string,
  password: boolean
}

