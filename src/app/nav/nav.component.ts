import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService,User } from '../user.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  isLoged:boolean = false;
  username:string = '';
  constructor(private userService:UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.userService.getLoggedUser()
                    .subscribe((res)=> {
                      let user = res as User;
                      this.username = user.name;                      
                      this.isLoged = true;
                    },
                    (err)=>{
                      this.username = '';
                      this.isLoged = false;
                    });
  }

  logout(){
    this.userService.logout()
    .subscribe(()=> {      
      this.router.navigate(["/login"])
    });
  }

}
