import { Component, OnInit } from '@angular/core';
import { TodoItem, TodoService } from '../todo.service';
import { UserService,User } from '../user.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  public myTodos: Array<TodoItem> = [];
  public isLoggedIn:boolean=false;
  public newTodoDescription:string='';

  constructor(private todoService:TodoService,
              private userService:UserService) { }

  ngOnInit(): void {    
    this.userService.getLoggedUser()
                    .subscribe((res)=> {
                      let user = res as User;
                      this.isLoggedIn = true;
                      
                    },
                    (err)=>{                      
                      this.isLoggedIn = false;
                    });

    this.getAllTodos();
  }

  getAllTodos(){
    this.todoService.getAll()
    .subscribe((res)=> this.myTodos = res as Array<TodoItem>);
  }

  updateTodo(todo:TodoItem):void{    
    this.todoService.updateTodo(todo)
                    .subscribe(()=> {
                      
                    });;    
  }

  removeTodo(todo:TodoItem):void{    
    this.todoService.removeTodo(todo.id)
                    .subscribe(()=> this.getAllTodos ());    
  }

  addNewTodo():void{
    console.log('addTodoClicked');
    let todo:TodoItem = {
      id:'',
      description: this.newTodoDescription,
      done: false 
    }    
    this.todoService.registerTodo(todo)
                    .subscribe(()=> {
                      this.newTodoDescription = "";  
                      //give 500ms delay to update the database before querying again
                      setTimeout(()=>{ this.getAllTodos() }, 500)                      
                    });

  }

}
