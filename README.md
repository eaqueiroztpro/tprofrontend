<p align="center">
  TPro Frontend Test
</p>

## Description

A Frontend Server build using: Built with angular

## Test

```bash
# Before build the docker image please test the code by running the following command:
$ npm run test
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# create the docker Image
$ npm run start

```


## Stay in touch

- Author - Eduardo Queiroz (eaqueiroz@gmail.com)

